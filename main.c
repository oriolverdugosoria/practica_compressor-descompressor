#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define BB while (getchar()!='\n')
#define MAX_TEXT 50
#define MAX_TEXT2 500
#define MAX_CARACTERS 50
#define MAX_REPETICIONS 50
#define MAX_CODI 50
#define MAX_DIGITS 4

void inicialitzacions(char text[],char llistaCar[], int llistaRepe[]);
void trobar_caracters_diferents_i_repeticions(char text[], char llistaCar[], int llistaRepe[], int *ill);
void ordenar(char llistaCar[], int llistaRepe[], int *ill);
void compactar(char text[], char llistaCar[], char codis[MAX_CODI+1][MAX_DIGITS+1],char textCompactat[], int *it, int *itc);
void printfs(int *it,int *ill, int *itc, char llistaCar[], int llistaRepe[], char codis[MAX_CODI+1][MAX_DIGITS+1]);
void descompactar(char textCompactat[], char codi[], char codis[MAX_CODI+1][MAX_DIGITS+1], char textDescompactat[], char llistaCar[], int *it2, bool *trobat);
void obtenirCodi(char codi[], char textCompactat[], int *ic, int *itc);
void saltaDollars(char textCompactat[], int *itc);
void cercaCodi(char codis[MAX_CODI+1][MAX_DIGITS+1], char codi[], bool *trobat, int *ic);
bool iguals(char par1[], char par2[]);

int main()
{
    char text[MAX_TEXT+1];
    char textCompactat[MAX_TEXT2+1];
    char textDescompactat[MAX_TEXT2+1];
    char llistaCar[MAX_CARACTERS+1];
    int llistaRepe[MAX_REPETICIONS];
    char codis[MAX_CODI+1][MAX_DIGITS+1]={"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
    char codi[MAX_DIGITS+1];
    bool trobat;
    int it, itc, ic, ill, it2=0;

    inicialitzacions(text,llistaCar, llistaRepe);

    printf("Intro text:");
    scanf("%20[^\n]", text); BB;

    trobar_caracters_diferents_i_repeticions(text, llistaCar,llistaRepe, &ill);

    ordenar(llistaCar, llistaRepe, &ill);

    compactar(text, llistaCar, codis, textCompactat, &it, &itc);
    printf("\n\nText compactat:\n\t%s", textCompactat);

    printfs(&it, &ill, &itc, llistaCar, llistaRepe, codis);

    descompactar(textCompactat, codi, codis, textDescompactat, llistaCar, &it2, &trobat);
    printf("\nEl text descompactat es :%s", textDescompactat);

    return 0;
}

/*
NOM:            inicialitzacions
DESCRIPCIÓ:     s'agafen els strings i s'inicialitza cada posició a '\0' 
RETORN:         retorna el text i les llistes inicialitzades amb '\0's
PRECONDICIÓ:    text[], llistaCar[] i llistaRepe han d'estar definides  
*/
void inicialitzacions(char text[],char llistaCar[], int llistaRepe[]){
    text[0]='\0';

    int i=0;
    while(i<=MAX_CARACTERS){
        llistaCar[i]='\0';
        i++;
    }
    i=0;
    while(i<MAX_CARACTERS){
        llistaRepe[i]=0;
        i++;
    }
}

/*
NOM:            trobar_caracters_diferents_i_repeticions
DESCRIPCIÓ:     es busca dins del text els diferents caràcters, si un caracter no és a la llista de caràcters s'afegeix i si ja hi es, s'augmenta ill per a que compti com a     repetició de caràcter   
RETORN:         retorna una llista de caràcters i una llista de repeticions de cada caràcter 
PRECONDICIÓ:    es necessita una llista de caràcters inicialitzada, un text i una llista de repeticions, ademés de uns indexs

EXEMPLE D'ÚS:   text="hola que tal";

                llistaCar="hola quet";
                llistaRepe="001110000";
*/
void trobar_caracters_diferents_i_repeticions(char text[], char llistaCar[], int llistaRepe[], int *ill){
    int it=0;
    while(text[it]!='\0'){
        //trobar caracters per la llistaCar
        *ill=0;
        while(text[it]!=llistaCar[*ill] &&
              llistaCar[*ill]!='\0')(*ill)++;
        //si no l'has trobat?
        if(llistaCar[*ill]=='\0'){
            llistaCar[*ill]=text[it];
        }else{
              llistaRepe[*ill]++;
        }
        it++;
    }
    while(llistaCar[*ill]!='\0')(*ill)++;
}

/*
NOM:            ordenar
DESCRIPCIÓ:     s'ordena la llista de repeticions amb la llista de caràcters per a que es trobin a la mateixa posició de totes les llistes gràcies al mètode de la bombolla    
RETORN:         retorna les dues llistes ordenades, de major aparició a menor
PRECONDICIÓ:    es necessiten les llistes amb els caràcters ja recollits i les repeticions de cada caràcter
*/
void ordenar(char llistaCar[], int llistaRepe[], int *ill){
    *ill=0;
    while(llistaCar[*ill]!='\0')(*ill)++;

    //metode de la bombolla
    int i; int j; int aux; char auxchar;
    for(i=2; i<= *ill; i++){
        for(j=0; j<= (*ill)-i; j++){
            if(llistaRepe[j] < llistaRepe[j+1]){
                aux = llistaRepe[j];
                llistaRepe[j] = llistaRepe[j+1];
                llistaRepe[j+1] = aux;
                auxchar = llistaCar[j];
                llistaCar[j] = llistaCar[j+1];
                llistaCar[j+1] = auxchar;
            }
        }
    }
    int it=0;
    printf("\nOrdre repeticio:");
    while(it<*ill){
        printf("\ncaracter %c te %d repeticions", llistaCar[it], llistaRepe[it]);
        it++;
    }
}

/*
NOM:            compactar
DESCRIPCIÓ:     es crea un text codificat agafant el codi que li correspongui a cada caràcter    
RETORN:         retorna un text compactat
PRECONDICIÓ:    es necessita que cada caràcter tingui assignat ja un codi de la llista codis i haver creat "textCompactat" per a ficar-hi codis

EXEMPLE D'ÚS:   text="hola";

                codis="0", "1", "01", "10";
                textCompactat="0$1$01$10;
*/
void compactar(char text[], char llistaCar[], char codis[MAX_CODI+1][MAX_DIGITS+1],char textCompactat[], int *it, int *itc){
    *it=0;
    *itc=0;
    while(text[*it]!='\0'){
        int i=0;
        while(llistaCar[i]!=text[*it]){
            i++;
        }
        int j=0;
        while(codis[i][j]!='\0'){
            textCompactat[*itc]=codis[i][j];
            j++; (*itc)++;
        }
        textCompactat[*itc]='$';
        (*itc)++;
        (*it)++;
    }
    textCompactat[*itc-1]='\0';
}

/*
NOM:            printfs
DESCRIPCIÓ:     aqui s'imprimeixen els resultats de les operacions per determinar quan ocupa el text en bits, el mateix amb el text compactat i veure quan s'estalvia amb la compactacio amb operacions matemàtiques. A continuació, es crea una llista amb tota la informació de la compactacio, amb cada caràcter diferent del text, els cops que surt repetit al text i el codi que li correspon per a poder comprovar si el codi assignat al text compactat es correcte.    
RETORN:         retorna les impresions dels resultats de la compactacio i una llista amb quin codi ha sigut assignat a cada caràcter
PRECONDICIÓ:    es necessita agafar el valor d'it, itc i ill per a fer les operacions ja que son les llargades del text(it), del textCompactat(itc) i de la quantitat de caràcters que hi ha al text(ill).  
*/
void printfs(int *it,int *ill, int *itc, char llistaCar[], int llistaRepe[], char codis[MAX_CODI+1][MAX_DIGITS+1]){
    float bits, perc;
    bits=(*it)*8;
    printf("\n\nBits del text: %d*8 = %.0lf bits", *it, bits);

    printf("\nBits del text compactat:%.01lf bits", (*itc)-1.00);

    perc=((bits-(*itc))/bits)*100;
    printf("\nPercentatge de compactacio: %.02lf %%", perc);

    printf("\nCaracter\t repeticions \t codi assignat");
    int i=0;
    while(i<(*ill)){
        printf("\n %c \t\t %d \t\t %s", llistaCar[i], llistaRepe[i], codis[i]);
        i++;
    }
}

/*
NOM:            descompactar
DESCRIPCIÓ:     s'agafa cada codi del text compactat i es busca a la llista de codis per a trobar el caràcter corresponent i copiar-lo al text descompactat     
RETORN:         retorna el text ,però ara descompactat
PRECONDICIÓ:    es necessita que el text compactat estigui complet, la creació d'un nou string "codi" i de "textDescompactat" per a copiar-hi els caràcters

EXEMPLE D'ÚS:   textcompactat="0$1$01";
                textDescompactat="caracter que te codi assignat 0", "caràcter que te codi assignat 1", caràcter que te codi assignat 01".
*/
void descompactar(char textCompactat[], char codi[], char codis[MAX_CODI+1][MAX_DIGITS+1], char textDescompactat[], char llistaCar[], int *it2, bool *trobat){
    int itc=0;
    while(textCompactat[itc]!='\0'){
        int ic=0;
        obtenirCodi(codi, textCompactat, &ic, &itc);
        saltaDollars(textCompactat, &itc);
        cercaCodi(codis, codi, &trobat, &ic);
        //descompactar
        if(trobat){
            textDescompactat[*it2]=llistaCar[ic];
            (*it2)++;
        }
    }
    textDescompactat[*it2]='\0';
}

/*
NOM:            obtenirCodi
DESCRIPCIÓ:     s'agafa cada codi de textCompactat, que es quan no hi ha ni '\0' ni '$' i es tracta d'un numero. Al final es fica un '\0' per evitar problemes.    
RETORN:         retorna un codi del textcompactat
PRECONDICIÓ:    es necessita que el text compactat estigui ben fet, amb sol numeros, dollars i un '\0' al final per a que funcioni correctament

EXEMPLE D'ÚS:   textCompactat="0$1";
                while(textCompactat[*itc]!='$' && textCompactat[*itc]!='\0'){
                codi[*ic]=textCompactat[*itc];
                (*ic)++;(*itc)++;
                }
                codi[*ic]='\0';
                codi="0";
*/
void obtenirCodi(char codi[], char textCompactat[], int *ic, int *itc){
        while(textCompactat[*itc]!='$' && textCompactat[*itc]!='\0'){
            codi[*ic]=textCompactat[*itc];
            (*ic)++;(*itc)++;
        }
        codi[*ic]='\0';
}

/*
NOM:            saltaDollars
DESCRIPCIÓ:     es salten els dollars i s'avança l'index del text compactat en cada cas    
RETORN:         retorna l'index del text compactat avançat  
PRECONDICIÓ:    es necessita el text compactat i un index del text compactat

EXEMPLE D'ÚS:   itc=3;
                //saltaDollars
                if(textCompactat[4]=='$')itc++;
                itc=4;
*/
void saltaDollars(char textCompactat[], int *itc){
    if(textCompactat[*itc]=='$') (*itc)++;
}

/*
NOM:            cercaCodi
DESCRIPCIÓ:     es busca el codi a la llista de codis    
RETORN:         retorna si ha trobat el codi o si no l'ha trobat
PRECONDICIÓ:    s'ha d'haver tret un codi del text compactat per poder comparar-lo amb la llista de codis
*/
void cercaCodi(char codis[MAX_CODI+1][MAX_DIGITS+1], char codi[], bool *trobat, int *ic){
        *ic=0;
        *trobat=false;
        while(codis[*ic][0]!='\0'){
            if(iguals(codi,codis[*ic])){*trobat=true;break;
            }else(*ic)++;
        }
}

/*
NOM:            iguals
DESCRIPCIÓ:     es comparen els codis caràcter a caràcter per a veure si son iguals    
RETORN:         retorna si dos paraules son iguals o no
PRECONDICIÓ:    es necessita un codi per a poder comparar-lo amb els codis de la llista de codis

EXEMPLE D'ÚS:   par1="hola"; par2="holas";
                if(par1[i]==par2[i]) iguals=true; //com no es igual l'ultim caràcter iguals no es true
                iguals=false;
                return iguals=false;
*/
bool iguals(char par1[], char par2[]){
    bool iguals=false;
    int i=0;
    while(par1[i]==par2[i] && (par1[i]!='\0' || par2[i]!='\0')) i++;
    if(par1[i]==par2[i]) iguals=true;
    return iguals;
}
