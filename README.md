# Practica_Compressor-Descompressor



Aquest és un codi per a optimitzar espai. Consta en veure quins son els caràcters més repetits de qualsevol text i convertir-los en codis que ocupin menys memòria.
Per exemple, agafant el caràcter més repetit de tots i ficant-li el codi 0, que es el que menys ocupa juntament amb el 1, assignat al segon caràcter més repetit.
És pot veure els bits que s'estalvia a l'hora de compactar un text d'aquesta manera.



Exemple:
![image.png](./image.png)
